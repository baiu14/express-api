import mongoose from "mongoose";

const Servers = mongoose.Schema({
	ip:{
		type: String,
		required: true
	},
	username:{
		type: String,
		required: true
	},
	sshkey:{
		type: String,
		required: true
	},
	userId:{
		type: String,
		required: true
	},
});
 
export default mongoose.model( 'Servers', Servers );