import mongoose from "mongoose";

const Apps = mongoose.Schema({
	serverId:{
		type: String,
		required: true
	},
	name:{
		type: String,
		required: true
	},
	username:{
		type: String,
		required: true
	},
	ip:{
		type: String,
		required: true
	},
});
 
export default mongoose.model( 'Apps', Apps );