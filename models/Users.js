import mongoose from "mongoose";

const Users = mongoose.Schema({
	username:{
		type: String,
		required: true
	},
	email:{
		type: String,
		required: true
	},
	password:{
		type: String,
		required: true
	},
	name:{
		type: String,
		required: true
	},
	country:{
		type: String,
		required: true
	}
});
 
export default mongoose.model( 'Users', Users );