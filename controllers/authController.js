import Users from "../models/Users.js";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
const tokenSecret = "rahasia";
 
export const register = async ( req, res ) => {
	const user = new Users( req.body );
	const salt = bcrypt.genSaltSync( 10 );
	user.password = bcrypt.hashSync( user.password, salt);
	try {
		const savedUser = await user.save();
		res.status( 200 ).json({
			"status": "success",
			"message": "Registration Success !",
			"data": null,
		});
	} catch ( error ) {
		res.status( 400 ).json({
			message: error.message
		});
	}
}
 
export const login = async ( req, res ) => {
	Users.findOne({"email": req.body.email })
	.then( user => {
		if ( ! user ) res.status( 400 ).json({
			"status" : "success",
			"message": "User not Found !",
			"data": null
		});
		else {
			bcrypt.compare( req.body.password, user.password, ( err, isMatch ) => {
				if ( err ) {
					res.status( 500 ).json({
						"status" : "success",
						"message": err,
						"data": null
					});
				} else if ( isMatch ) {
					res.status( 200 ).json({
						"status" : "success",
						"message": "Login Success !",
						"data": {
							'token': generateToken( user ),
							'id': user._id,
							'name': user.name,
							'username': user.username,
							'email': user.email,
							'country': user.country,
						}
					});
				} else {
					res.status( 403 ).json({
						"status" : "success",
						"message": "Password do not Match !",
						"data": null
					});
				}
			});
		}
	});
}

function generateToken( user ) {
	return jwt.sign({data: user}, tokenSecret, {expiresIn: '24h'})
}
