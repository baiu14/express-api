import Apps from "../models/Apps.js";
import Servers from "../models/Servers.js";
 
export const getApp = async ( req, res ) => {
	try {
		const app = await Apps.find();
		res.json( app );
	} catch ( error ) {
		res.status( 500 ).json({
			message: error.message
		});
	}
}
 
export const saveApp = async ( req, res ) => {
	const ip = req.body.ip;
	const server = await Servers.findOne({"ip": ip})
	let data = {
		ip: ip,
		name: req.body.name,
		username: server.username,
		serverId: server._id
	}
	const app = new Apps( data );
	try {
		const savedApp = await app.save();
		res.status( 201 ).json( savedApp );
	} catch ( error ) {
		res.status( 400 ).json({
			message: error.message
		});
	}
}
 
export const updateApp = async ( req, res ) => {
	const cekId = await Apps.findById( req.params.id );
	if( ! cekId ) return res.status( 404 ).json({
		message: "Data tidak ditemukan"
	}); 
	try {
		const updatedApp = await Apps.updateOne(
			{
				"_id": req.params.id
			}, 
			{
				$set: req.body
			}
		);
		res.status( 200 ).json( updatedApp );
	} catch ( error ) {
		res.status( 400 ).json({
			message: error.message
		});
	}
}
 
export const deleteApp = async ( req, res ) => {
	const cekId = await Apps.findById( req.body );
	if( ! cekId ) return res.status( 404 ).json({
		message: "Data tidak ditemukan"
	});
	try {
		const deletedApp = await Apps.deleteOne({
			"_id": req.body._id
		});
		res.status( 200 ).json( deletedApp );
	} catch ( error ) {
		res.status( 400 ).json({
			message: error.message
		});
	}
}

export const searchApp = async ( req, res ) => {
	try {
		const app = await Apps.find({"name": { $regex: req.query.name }});
		res.json( app );
	} catch ( error ) {
		res.status( 500 ).json({
			message: error.message
		});
	}
}
