import Servers from "../models/Servers.js";
 
export const getServer = async ( req, res ) => {
	try {
		const server = await Servers.find();
		res.json( server );
	} catch ( error ) {
		res.status( 500 ).json({
			message: error.message
		});
	}
	 
}

export const getServerByUser = async ( req, res ) => {
	try {
		const server = await Servers.find({userId: req.query.userId});
		res.json( server );
	} catch ( error ) {
		res.status( 500 ).json({
			message: error.message
		});
	}
}
 
export const saveServer = async ( req, res ) => {
	console.log( req.body )
	const server = new Servers( req.body );
	try {
		const savedServer = await server.save();
		res.status( 201 ).json( savedServer );
	} catch ( error ) {
		res.status( 400 ).json({
			message: error.message
		});
	}
}
 
export const updateServer = async ( req, res ) => {
	const cekId = await Servers.findById( req.params.id );
	if( ! cekId ) return res.status( 404 ).json({
		message: "Data tidak ditemukan"
	}); 
	try {
		const updatedServer = await Servers.updateOne(
			{
				"_id": req.params.id
			}, 
			{
				$set: req.body
			}
		);
		res.status( 200 ).json( updatedServer );
	} catch ( error ) {
		res.status( 400 ).json({
			message: error.message
		});
	}
}
 
export const deleteServer = async ( req, res ) => {
	const cekId = await Servers.findById( req.body );
	if( ! cekId ) return res.status( 404 ).json({
		message: "Data tidak ditemukan"
	});
	try {
		const deletedServer = await Servers.deleteOne({
			"_id": req.body._id
		});
		res.status( 200 ).json( deletedServer );
	} catch ( error ) {
		res.status( 400 ).json({
			message: error.message
		});
	}
}

export const searchServer = async ( req, res ) => {
	try {
		const server = await Servers.find({"ip": { $regex: req.query.ip }});
		res.json( server );
	} catch ( error ) {
		res.status( 500 ).json({
			message: error.message
		});
	}
}