import Users from "../models/Users.js";
 
export const getUser = async ( req, res ) => {
	try {
		const user = await Users.find();
		res.json( user );
	} catch ( error ) {
		res.status( 500 ).json({
			message: error.message
		});
	}	 
}
 
export const getUserById = async ( req, res ) => {
	try {
		const user = await Users.findById( req.params.id );
		res.json( user );
	} catch ( error ) {
		res.status( 404 ).json({
			message: error.message
		});
	}
}
 
export const saveUser = async ( req, res ) => {
	const user = new Users( req.body );
	try {
		const savedUser = await user.save();
		res.status( 201 ).json({
			'message': 'Registration Success !'
		});
	} catch ( error ) {
		res.status( 400 ).json({
			message: error.message
		});
	}
}
 
export const updateUser = async ( req, res ) => {
	const cekId = await Users.findById( req.params.id );
	if( ! cekId ) return res.status( 404 ).json({
		message: "Data tidak ditemukan"
	}); 
	try {
		const updatedUser = await Users.updateOne(
			{
				"_id": req.params.id
			}, 
			{
				$set: req.body
			}
		);
		res.status( 200 ).json( updatedUser );
	} catch ( error ) {
		res.status( 400 ).json({
			message: error.message
		});
	}
}
 
export const deleteUser = async ( req, res ) => {
	const cekId = await Users.findById( req.params.id );
	if( ! cekId ) return res.status( 404 ).json({
		message: "Data tidak ditemukan"
	});
	try {
		const deletedUser = await Users.deleteOne({
			"_id": req.params.id
		});
		res.status( 200 ).json( deletedUser );
	} catch ( error ) {
		res.status( 400 ).json({
			message: error.message
		});
	}
}

export const searchUser = async ( req, res ) => {
	try {
		const user = await Users.find({"name": { $regex: req.query.name }});
		res.json( user );
	} catch ( error ) {
		res.status( 500 ).json({
			message: error.message
		});
	}
}
