import express from "express";
import { 
	register, 
	login } from "../controllers/authController.js";

import { 
	getUser, 
	getUserById,
	saveUser, 
	updateUser,
	deleteUser,
	searchUser } from "../controllers/userController.js";
import { 
	getServer, 
	getServerByUser, 
	saveServer, 
	updateServer,
	deleteServer, 
	searchServer } from "../controllers/serverController.js";
import { 
	getApp, 
	saveApp, 
	updateApp,
	deleteApp,
	searchApp } from "../controllers/appController.js";
 
const router = express.Router();
 
router.post( '/register', register );
router.post( '/login', login );

router.get( '/users', getUser );
router.get( '/users/findid/:id', getUserById );
router.post( '/users/create', saveUser );
router.post( '/users/update/:id', updateUser );
router.post( '/users/remove', deleteUser );
router.get( '/users/search', searchUser );

router.get( '/servers', getServer );
router.get( '/servers/byuser', getServerByUser );
router.post( '/servers/create', saveServer  );
router.post( '/servers/update/:id', updateServer );
router.post( '/servers/remove', deleteServer );
router.get( '/servers/search', searchServer );

router.get( '/apps', getApp );
router.post( '/apps/create', saveApp );
router.post( '/apps/update/:id', updateApp );
router.post( '/apps/remove', deleteApp );
router.get( '/apps/search', searchApp );

export default router;